<?php

namespace App\Repository;

use App\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Product::class);
    }
     /**
      * @return ProductCart[] Returns an array of Product objects
      */
    public function findArray($array)
    {
        $qb=$this->createQueryBuilder('u')
            ->select('u')
            ->where('u.id IN (:array)')
            ->setParameter('array', $array)
            ->getQuery()
            ->getResult();
    }


    
     /**
      * @return Product[] Returns an array of Product objects
      */
    
    public function findByCategorie($categorie)
    {
        return $this->createQueryBuilder('v')            
            ->select('v')
            ->andWhere('v.categorie = :categorie')
            ->setParameter('categorie', $categorie)
            ->orderBy('v.categorie', 'desc')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
      * @return ProductByTag[] Returns an array of Product objects
      */
    
      public function myFindByTag($tag)
      {
          return $this->createQueryBuilder('t') 
              ->innerJoin('t.tags', 'g') 
              ->where('g.id = :tag_id')
              ->setParameter('tag_id', $tag)
              ->orderBy('g.id', 'ASC')
              ->setMaxResults(10)
              ->getQuery()
              ->getResult()
          ;
      }
    
    //   $query = $repository->createQueryBuilder('u')
    //   ->innerJoin('u.groups', 'g')
    //   ->where('g.id = :group_id')
    //   ->setParameter('group_id', 5)
    //   ->getQuery()->getResult();

    /*
    public function findOneBySomeField($value): ?Product
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
    public function search($term)
    {
        return $this->createQueryBuilder('Product')
            ->andWhere('Product.name LIKE :lib')
            ->setParameter('lib', '%'.$term.'%')
            ->getQuery()
            ->execute();
    }
}
