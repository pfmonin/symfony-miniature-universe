<?php

namespace App\DataFixtures;

use App\Entity\Product;
use App\Entity\Categorie;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;

class ProductFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
      $faker = \Faker\Factory::create('fr_FR');

      // Creer 3 catégorie Faker

      for($l=1; $l<= 5; $l++){
        $categorie = new Categorie();
        $categorie ->setName($faker->word(1, true))
                   ->setDescription($faker->paragraph())
                   ->setSlug($faker->word(3, true));

        $manager->persist($categorie);

      //   // Creer entre 4 et 6 products

        for ($j=1; $j<=mt_rand(4, 7); $j++){
          $product=new Product();       

          $product->setName($faker->word(2, true));                  
          $product->setImageName($faker->word(1,true));
          $product->setImageSize(250);
          $product->setSlug($faker->word(1,true));
          $product->setDescription($faker-> paragraph(2));
          $product->setPrix($faker-> numberBetween (10, 199));                  
          $product->setCreatedAt($faker->dateTimeAD($max = 'now', $timezone = null));
          $product->setUpdatedAt($faker->dateTimeAD($max = 'now', $timezone = null));
          $product->setCategorie($categorie);
          $product->setStock(1);


          $manager->persist($product);
         
        }

      }


      $manager->flush();
  }
}
