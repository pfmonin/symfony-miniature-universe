<?php

namespace App\DataFixtures;

use App\Entity\User;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Faker\Factory;

class UserFixtures extends Fixture
{
     /**
     * @var UserPasswordEncoderInterface
    */
    private $passwordEncoder;

     public function __construct(UserPasswordEncoderInterface $passwordEncoder)
     {
         $this->passwordEncoder = $passwordEncoder;
     }

    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');
        $user = new User();
        $user->setEmail('orou.dev@gmail.com');
        $user->setUsername('Pierre Monin');
        $password = $this->passwordEncoder->encodePassword($user, '123');
        $user->setPassword($password);
        $user->setImageName($faker->imageUrl($width=350, $height=250, 'cats', true, 'Faker'));
        $user->setImageSize(250);
        $rolesAdmin = ['ROLE_ADMIN'];
        $user->setRoles($rolesAdmin);
        $user->setCreatedAt($faker->dateTimeAD($max = 'now', $timezone = null));
        $user->setUpdatedAt($faker->dateTimeAD($max = 'now', $timezone = null));
        $manager->persist($user);

        $faker = Factory::create('fr_FR');
        $user = new User();
        $user->setEmail($faker->email());
        $user->setUsername('Loane');
        $password = $this->passwordEncoder->encodePassword($user, 'loane');
        $user->setPassword($password);
        $user->setImageName($faker->imageUrl($width=350, $height=250, 'cats', true, 'Faker'));
        //$user->setImageName($faker->word(1,true));        
        $user->setImageSize(250);
        $rolesAdmin = ['ROLE_USER'];
        $user->setRoles($rolesAdmin);
        $user->setCreatedAt($faker->dateTimeAD($max = 'now', $timezone = null));
        $user->setUpdatedAt($faker->dateTimeAD($max = 'now', $timezone = null));
        $manager->persist($user);

        // $faker = Factory::create('fr_FR');
        // for ($i=0; $i<3; $i++)
        // {
        // $user = new User();
        // $user->setEmail($faker->email());
        // $user->setUsername('Pierre Monin');
        // $password = $this->passwordEncoder->encodePassword($user, $faker -> password());
        // $user->setPassword($password);
        // $user->setImage($faker -> imageUrl($width = 200, $height = 250));
        // $rolesAdmin[] = 'ROLE_ADMIN';
        // $user->setRoles($rolesAdmin);
        // $manager->persist($user);
        // }

        $faker = Factory::create('fr_FR');
        for ($k=0; $k<10; $k++)
        {
            $user = new User();
            $user ->setUsername($faker -> userName())
                  ->setEmail($faker->email());                  
            $password = $this->passwordEncoder->encodePassword($user, $faker -> password());
            $user ->setPassword($password); 
            $user->setImageName($faker->imageUrl($width=350, $height=250, 'cats', true, 'Faker'));
            $user->setImageName($faker->word(1,true));
            $user->setImageSize(250);
            // $rolesUser=[] 
            $rolesUser = ['ROLE_USER'];
            $user->setRoles($rolesUser); 
            $user->setCreatedAt($faker->dateTimeAD($max = 'now', $timezone = null));
            $user->setUpdatedAt($faker->dateTimeAD($max = 'now', $timezone = null));             
                  
            $manager->persist($user);
        }
        
        $manager->flush();           
        
    }
   
}
