<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Vich\UploaderBundle\Form\Type\VichImageType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;

class UserEditType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class, [
                'label' => 'Votre email :',
                'required' => false,
                'attr' => ['autocomplete' => 'disabled'
            ]]) 

            ->add('password', RepeatedType::class, [
                'type' => PasswordType::class,
                'invalid_message' => 'Les mots de passe doivent correspondre',
                'options' => ['attr' => ['class' => 'password-field']],
                'required' => true,
                'first_options'  => ['label' => 'Votre mot de passe :'],
                'second_options' => ['label' => 'Repetez votre mot de passe :']
            ])

            ->add('imageFile', VichImageType::class, [
                'label' => 'Votre image :',
                'required' => false,
                'allow_delete' => false,
                'download_label' => '...',
                'download_uri' => false,                            
            ])            

            ->add('username', TextType::class, [
                'label' => 'Votre nom d\'utilisateur :',
                'required' => false,
                'attr' => [
                    'autocomplete' => 'disabled'
            ]])
                        
            ->add('name', TextType::class, [
                'label' => 'Votre nom :',
                'required' => false,
                'attr' => [
                    'autocomplete' => 'disabled'
                    ]])
            ->add('firstname', TextType::class, [
                'label' => 'Votre prenom :',
                'required' => false,
                'attr' => [
                    'autocomplete' => 'disabled'
                    ]])
            ->add('adress', TextType::class, [
                'label' => 'Votre adresse :',
                'required' => false,
                'attr' => [
                    'autocomplete' => 'disabled'
            ]])
            ->add('zipcode', TextType::class, [
                'label' => 'Votre code postal :',
                'required' => false,
                'attr' => [
                    'autocomplete' => 'disabled'
            ]])
            ->add('city', TextType::class, [
                'label' => 'Votre ville de résidence :',
                'required' => false,
                'attr' => [
                    'autocomplete' => 'disabled'
            ]])
            ->add('state', TextType::class, [
                'label' => 'Votre état de résidence  :',
                'required' => false,
                'attr' => [
                    'autocomplete' => 'disabled'
            ]])
            ->add('country', TextType::class, [
                'label' => 'Votre pays de résidence  :',
                'required' => false,
                'attr' => [
                    'autocomplete' => 'disabled'
            ]])            
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}



    

// ->add('roles', ChoiceType::class,[
// 	"choices" => [
// 		'Administrateur' => "ROLE_ADMIN",
// 		'Administrateur' => "ROLE_ADMIN"
        
// 	],
// ])
    
