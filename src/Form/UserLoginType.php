<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserLoginType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email')
            ->add('roles')
            ->add('password')
            // ->add('username')
            // ->add('image')
            // ->add('imageName')
            // ->add('imageSize')
            // ->add('updatedAt')
            // ->add('createdAt')
            // ->add('name')
            // ->add('firstname')
            // ->add('adress')
            // ->add('zipcode')
            // ->add('city')
            // ->add('state')
            // ->add('country')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
