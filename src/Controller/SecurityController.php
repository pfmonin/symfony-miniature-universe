<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserLoginType;
use App\Form\RegistrationType;
use App\Form\ShippingFormType;
use App\Form\UserCreateFormType;
use App\Repository\CategorieRepository;
use Symfony\Component\Form\PasswordType;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;


class SecurityController extends AbstractController
{
   
    /**
     * @Route("/login", name="app_login", methods={"GET", "POST"})
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        $error = $authenticationUtils->getLastAuthenticationError();      
        $lastUsername = $authenticationUtils->getLastUsername();
       
        return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
    }

     /**
     * @Route("/user-registration", name="app_registration_user", methods={"GET", "POST"})
     */
    public function registration(AuthenticationUtils $authenticationUtils, Request $request, UserPasswordEncoderInterface $encoder, CategorieRepository $repoCategorie): Response
    {
        $categories = $repoCategorie->findAll();
        $manager = $this->getDoctrine()->getManager();        

        $user = new User();
        $formRegistration = $this->createForm(UserCreateFormType::class, $user);  
        $formRegistration->handleRequest($request);  
          if ($formRegistration->isSubmitted() && $formRegistration->isValid()){
  
            $hash = $encoder->encodePassword($user, $user->getPassword());
            $user -> setPassword($hash);
            $rolesUser[] = 'ROLE_USER';
            $user->setRoles($rolesUser);
            $user->setImageSize(null);
            $manager->persist($user);
            $manager->flush();
  
            return $this->redirectToRoute('home/');
  
      }
      return $this->render('security/createUserForm.html.twig', [
        'title_page'=> 'Create a new account',
        'formRegistration'=> $formRegistration -> createView(),
        'categories'=>$categories
        ]);
    }

    /**
     * @Route("/forgotten_password", name="app_forgotten_password")
     */
    public function forgottenPassword(
        Request $request,
        UserPasswordEncoderInterface $encoder,
        \Swift_Mailer $mailer,
        TokenGeneratorInterface $tokenGenerator,
        CategorieRepository $repoCategorie
    ): Response
    {
        $categories = $repoCategorie->findAll();

        if ($request->isMethod('POST')) {           
           

            $email = $request->request->get('email');

            $entityManager = $this->getDoctrine()->getManager();
            $user = $entityManager->getRepository(User::class)->findOneByEmail($email);
            /* @var $user User */

            if ($user === null) {
                $this->addFlash('danger', 'Email Inconnu');
                return $this->redirectToRoute('home');
            }
            $token = $tokenGenerator->generateToken();

            try{
                $user->setResetToken($token);
                $entityManager->flush();
            } catch (\Exception $e) {
                $this->addFlash('warning', $e->getMessage());
                return $this->redirectToRoute('home');
            }

            $url = $this->generateUrl('app_reset_password', array('token' => $token), UrlGeneratorInterface::ABSOLUTE_URL);

            $message = (new \Swift_Message('Forgot Password'))
                ->setFrom('orou.dev@gmail.com')
                ->setTo($user->getEmail())
                ->setBody(
                    "blablabla voici le token pour reseter votre mot de passe : " . $url,
                    'text/html'
                );

            $mailer->send($message);

            $this->addFlash('notice', 'Mail envoyé');

            return $this->redirectToRoute('home');
        }

        return $this->render('security/forgotten_password.html.twig', [
            'categories'=>$categories
        ]);
    }

        /**
     * @Route("/reset_password/{token}", name="app_reset_password")
     */
    public function resetPassword(Request $request, string $token, UserPasswordEncoderInterface $passwordEncoder)
    {

        if ($request->isMethod('POST')) {
            $entityManager = $this->getDoctrine()->getManager();

            $user = $entityManager->getRepository(User::class)->findOneByResetToken($token);
            /* @var $user User */

            if ($user === null) {
                $this->addFlash('danger', 'Token Inconnu');
                return $this->redirectToRoute('home');
            }

            $user->setResetToken(null);
            $user->setPassword($passwordEncoder->encodePassword($user, $request->request->get('password')));
            $entityManager->flush();

            $this->addFlash('notice', 'Mot de passe mis à jour');

            return $this->redirectToRoute('home');
        }else {

            return $this->render('security/reset_password.html.twig', ['token' => $token]);
        }

    }

     /**
     * @Route("/logout", name="app_logout", methods={"GET"})
     */
    public function logout(){}
}
