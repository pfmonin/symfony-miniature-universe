<?php

namespace App\Controller;

use App\Entity\Product;
use App\Entity\Categorie;
use App\Repository\ProductRepository;
use App\Repository\CategorieRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class CategorieController extends AbstractController
{
    /**
     * @Route("/categorie", name="categorie")
     */
    public function index(CategorieRepository $repo)
    {
        $user = $this->getUser();
        $categories = $repo->findAll();        

        return $this->render('header/nav.html.twig', [
            'controller_name' => 'CategorieController',
            'categories' => $categories
        ]);
    }

     /**
     * @Route("/home/categorie/{id}", name="app_categorie_show")
     */
    public function categorie_show(ProductRepository $repoProduct, CategorieRepository $repo, $id, Categorie $categorie)
    { 
        $user = $this->getUser();
        $categories = $repo->findAll();
        $products = $repoProduct->findByCategorie($id);
        
        return $this->render('home/app_category_show.html.twig', [
        // 'title_page' =>$categorie->getName(),
        'title_page'=>$categorie->getName(),
        'categories' => $categories,        
        'products'=>$products,
        
                  
        ]);
    }
}
