<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\ArticleComment;
use App\Form\ArticleCommentType;
use App\Repository\ArticleRepository;
use App\Repository\CategorieRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class BlogController extends AbstractController
{
    /**
     * @Route("/blog", name="app_blog")
     */
    public function index(ArticleRepository $repo, CategorieRepository $repoCategorie)
    {
        $user = $this->getUser();          
        $articles = $repo->findAll(); 
        $categories = $repoCategorie->findAll();     

        return $this->render('blog/index.html.twig', [
            'title_page' => 'Les news',
            'user' => $user, 
            'articles'=> $articles, 
            'categories'=>$categories
            
        ]);
    }

     /**
     * @Route("/blog/{id}", name="app_blog_show")
     */
    public function blog_show(ArticleRepository $repo, Article $article, Request $request, CategorieRepository $repoCategorie )
    {  
        $user = $this->getUser();
        $categories = $repoCategorie->findAll();     
 
        $comment = new ArticleComment();         
        $form = $this->createForm(ArticleCommentType::class,$comment); 

        $form->handleRequest($request);
        if ($form-> isSubmitted() && $form-> isValid()){
          $comment->setCreatedAt(new \DateTime())
                  ->setArticle($article);

          $manager = $this->getDoctrine()->getManager();

          $manager->persist($comment);
          $manager->flush();

          return $this->redirectToRoute('app_blog_show', [ 'id' => $article->getId()]);
    }

        if(!$article){
            return new NotFoundHttpException("L'article avec l'ID " .$id. "n'existe pas");
        }

        return $this->render('blog/blog_show.html.twig', [
        'title_page' => 'Détail du produit: ' .$article->getTitle(),
        'categories'=>$categories,
        'article'=>$article,
        'commentForm'=>$form->createView(),
                    
        ]);
    }
}
