<?php

namespace App\Controller;

use App\Repository\ProductRepository;
use App\Repository\CategorieRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class CartController extends AbstractController
{
    /**
     * @Route("/cart", name="cart")
     */
    public function index(SessionInterface $session, ProductRepository $repoProduct, CategorieRepository $repoCategorie)
    {

        $user = $this->getUser();        
        $categories = $repoCategorie->findAll();  

         $panier = $session->get('panier', []);

         $panierWithData = [];

         foreach($panier as $id=>$quantity){
             $panierWithData[]=[
                 'product'=>$repoProduct->find($id),
                 'quantity'=>$quantity
             ];
         }

         $total = 0;

         foreach($panierWithData as $item){
             $totalItem = $item['product']->getPrix() * $item['quantity'];
             $total += $totalItem;
         }

        return $this->render('cart/index.html.twig', [
            'categories'=>$categories,
            'items'=>$panierWithData,
            'total'=>$total
        ]);
    }

    /**
     * @Route("/cart/add/{id}", name="cart_add")
     */
    public function add($id, SessionInterface $session )
    {
        $panier = $session->get('panier', []);

        if(!empty($panier[$id])){
            $panier[$id]++;
        }else{
           
            $panier[$id] = 1;
        }
        $session->set('panier', $panier);

        return $this->redirectToRoute("cart");
        
        // dd($session->get('panier'));
    }

    /**
     * @Route("/cart/remove/{id}", name="cart_remove")
     */
    public function remove($id, SessionInterface $session)
    {
        $panier = $session->get('panier', []);

        if(!empty($panier[$id])){
            unset($panier[$id]);
        }

        $session->set('panier', $panier);

        return $this->redirectToRoute("cart");
    }
}
