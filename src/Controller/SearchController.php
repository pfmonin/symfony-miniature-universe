<?php

namespace App\Controller;

use App\Entity\Search;
use App\Repository\ProductRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class SearchController extends AbstractController
{
    /**
     * @Route("/search", name="search")
     */
    public function search(ProductRepository $repoProduct, Request $request)
    {
        $product = new Search();
        $form =  $this->createFormBuilder($product)
            ->add('name', TextType::class, array('attr'=> array('class'=> 'form-control')))
            ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted()){
            $term = $product->getName();
            $allProduct = $repoProduct->search($term);
        }
        else{
            $allProduct = $repoProduct->findAll();
        }
    }
}
