<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use EasyCorp\Bundle\EasyAdminBundle\Controller\EasyAdminController;

class AdminController extends EasyAdminController
{
    /**
    * @Route("/admin", name="app_admin")
    */
    public function index()
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN');        
        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'User tried to access a page without having ROLE_ADMIN');

        $user = $this -> getUser();        

        return $this->render('admin/index.html.twig', [
            'user' => $user,
            
        ]);
    }
}
