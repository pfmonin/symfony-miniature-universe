<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\ProfileType;
use App\Form\UserEditType;
use App\Form\ResetPasswordType;
use Symfony\Component\Form\FormError;
use App\Repository\CategorieRepository;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;

class UserAccountController extends AbstractController
{
    /**
     * @Route("/user/account", name="user_account")
     */
    public function AccountAction(CategorieRepository $repoCategorie)
    {
        $user = $this->getUser();
        $categories = $repoCategorie->findAll();

        return $this->render('user_account/index.html.twig', [
            'title_page' => 'Welcome Pirate',
            'user'=>$user,
            'categories'=>$categories,
             
        ]);
    }

    /**
     * @Route("/user/account/resetpw", name="user_reset_pw")
     */
    public function editPwAction(Request $request, CategorieRepository $repoCategorie)
    {
        $categories = $repoCategorie->findAll();
    	$em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
    	$form = $this->createForm(ResetPasswordType::class, $user);

    	$form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $passwordEncoder = $this->get('security.password_encoder');
            $oldPassword = $request->request->get('etiquettebundle_user')['oldPassword'];

            // Si l'ancien mot de passe est bon
            if ($passwordEncoder->isPasswordValid($user, $oldPassword)) {
                $newEncodedPassword = $passwordEncoder->encodePassword($user, $user->getPassword());
                $user->setPassword($newEncodedPassword);
                
                $em->persist($user);
                $em->flush();

                $this->addFlash('notice', 'Votre mot de passe à bien été changé !');

                return $this->redirectToRoute('app_home');
            } else {
                $form->addError(new FormError('Ancien mot de passe incorrect'));
            }
        }
    	
    	return $this->render('user_account/resetpw.html.twig', array(
            'form' => $form->createView(),
            'categories'=>$categories,
    	));
    }

    /**
     * @Route("/user/account/edit/{id}", name="user_edit")
     */
    public function editUserAction(Request $request, ObjectManager $manager, $id, UserPasswordEncoderInterface $passwordEncoder)
    {
        $user = $this->getUser();
        $user = new User();
        $user = $this->getDoctrine()->getRepository
        (User::class)->find($id);
            
        $form = $this->createForm(UserEditType::class, $user);
        $form->handleRequest($request);
           
        if ($form->isSubmitted() && $form->isValid()){
            
            $user->setPassword(
                $passwordEncoder->encodePassword(
                    $user,
                    $form->get('password')->getData()
                )
            );
            
            
           $manager = $this->getDoctrine()->getManager();
           $manager->persist($user);
           $manager->flush();
           return $this->redirectToRoute('user_account');
            
        }   
        return $this->render('user_account/editUser.html.twig',[
            'form' => $form->createView(),
            'user' =>$user,
        ]);
    }
}
