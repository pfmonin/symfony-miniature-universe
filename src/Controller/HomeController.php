<?php

namespace App\Controller;

use App\Entity\Tag;
use App\Entity\Product;
use App\Entity\ImageCarousel;
use App\Entity\ProductComment;
use App\Form\ProductCommentType;
use App\Repository\TagRepository;
use App\Repository\ArticleRepository;
use App\Repository\ProductRepository;
use App\Repository\CategorieRepository;
use App\Repository\ImageCarouselRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class HomeController extends AbstractController
{
    /**
     ** @Route("/", name="home")
     *  @Route("/home")     
     */
    public function index(ProductRepository $repo, ImageCarouselRepository $repoCarousel, CategorieRepository $repoCategorie, ArticleRepository $repoArticle)
    {   
        $user = $this->getUser();        
        $categories = $repoCategorie->findAll();       
        $imagesCarousel = $repoCarousel->findAll();    
        // $latest = $repo->findBy([], ['createdAt' => 'ASC'], '3');        
        $latest = $repo->myFindByTag(1); 
        $coupDeCoeur = $repo->myFindByTag(2);         
        $productHome=$repo->myFindByTag(4);


        $posts=$repoArticle->findBy([], ['createdAt'=>'DESC'],'4');

               

        return $this->render('home/index.html.twig', [
            'title_page' => ' MU\'s Shop 🎨',            
            'imagesCarousel'=>$imagesCarousel,
            'latest' => $latest,
            'coupDeCoeur' => $coupDeCoeur, 
            'categories'=>$categories, 
            'productHome'=>$productHome,
            'posts'=>$posts
            
                       
        ]);  
    }
    
    /**
     * @Route("/home/{id}", name="app_product_show")
     */
    public function product_show(ProductRepository $repo, Product $product, Request $request, CategorieRepository $repoCategorie)
    {  
        $user = $this->getUser();
        $categories = $repoCategorie->findAll();  
        $comment = new ProductComment();         
        $form = $this->createForm(ProductCommentType::class,$comment); 

        $form->handleRequest($request);
        if ($form-> isSubmitted() && $form-> isValid())
        {
          $comment->setCreatedAt(new \DateTime())
                  ->setProduct($product);
          $manager = $this->getDoctrine()->getManager();

          $manager->persist($comment);
          $manager->flush();
          return $this->redirectToRoute('app_product_show', [ 'id' => $product->getId()]);
        }     
        if(!$product){
            return new NotFoundHttpException("Le produit avec l'ID " .$id. "n'existe pas");
        }
        return $this->render('home/app_product_show.html.twig', [
        'title_page' =>$product->getName(),
        'categories'=>$categories,
        'product'=>$product,
        'commentForm'=>$form->createView(),           
        ]);
    }
}
